#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "analyzer.h"
#include "arraylist.h"


#define MAX 1000000

int main(int argc, char ** argv){
	int tasNormal[MAX];
	int i;
	int valeur;
	// Analyse du temps pris par les opérations.
  	analyzer_t * time_analysis = analyzer_create();
  	// Analyse du nombre de copies faites par les opérations.
  	analyzer_t * copy_analysis = analyzer_create();
  	// Analyse de l'espace mémoire inutilisé.
  	analyzer_t * memory_analysis = analyzer_create(); 
  	// Mesure de la durée d'une opération.
  	struct timespec before, after;
  	clockid_t clk_id = CLOCK_REALTIME;
	srand(11400043);
  	// utilisé comme booléen pour savoir si une allocation a été effectuée.
  	char memory_allocation;
	for (i = 0 ; i < MAX; i ++){
		clock_gettime(clk_id, &before);
    	insertion_tas_binaire(tasNormal, rand(), i);
    	clock_gettime(clk_id, &after);
		// Enregistrement du temps pris par l'opération
		analyzer_append(time_analysis, after.tv_nsec - before.tv_nsec);
	}
	for (i = 0; i < MAX ; i ++){
		printf("%d\n", tasNormal[i]);
	}
	// Affichage de quelques statistiques sur l'expérience.
	fprintf(stderr, "Total cost: %Lf\n", get_total_cost(time_analysis));
  	fprintf(stderr, "Average cost: %Lf\n", get_average_cost(time_analysis));
  	fprintf(stderr, "Variance: %Lf\n", get_variance(time_analysis));
  	fprintf(stderr, "Standard deviation: %Lf\n", get_standard_deviation(time_analysis));
	// Sauvegarde les données de l'expérience.
	save_values(time_analysis, "plots/dynamic_array_time_c.plot");
	
	// Nettoyage de la mémoire avant la sortie du programme
	analyzer_destroy(time_analysis);
  	analyzer_destroy(copy_analysis);
  	analyzer_destroy(memory_analysis);
  	return EXIT_SUCCESS;
}
