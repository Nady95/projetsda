#include<stdio.h>
#include <time.h>
#include<stdlib.h>
#include "arraylist.h"
#include "analyzer.h"


int main(int argc, char ** argv){
  int i;
  double r;
  // Tableau dynamique.
  arraylist_t * a = arraylist_create();
  // Analyse du temps pris par les opérations.
  analyzer_t * time_analysis = analyzer_create();
  // Analyse du nombre de copies faites par les opérations.
  analyzer_t * copy_analysis = analyzer_create();
  // Analyse de l'espace mémoire inutilisé.
  analyzer_t * memory_analysis = analyzer_create(); 
  struct timespec before, after;
  clockid_t clk_id = CLOCK_REALTIME;
  // utilisé comme booléen pour savoir si une allocation a été effectuée.
  char memory_allocation; 
  srand(11400043);

  for(i = 0; i < 1000000 ; i++){
    // Un nombre aleatoire entre 0 et 1
    r = (double)rand()/(double)RAND_MAX;
    // Ajout d'un élément et mesure du temps pris par l'opération.
    
    if( r <= 0.5){
      clock_gettime(clk_id, &before);
      insertion_tas_binaire(a, i);
      clock_gettime(clk_id, &after); 
    }
    //Supprimer un element
    else{
      timespec_get(&before, TIME_UTC);
      extract_min(a);
      timespec_get(&after, TIME_UTC);
    }
    // Enregistrement du temps pris par l'opération
    analyzer_append(time_analysis, after.tv_nsec - before.tv_nsec);
    // Enregistrement du nombre de copies efféctuées par l'opération.
    // S'il y a eu réallocation de mémoire, il a fallu recopier tout le tableau.
    analyzer_append(copy_analysis, (memory_allocation)? arraylist_size(a):1 );
    // Enregistrement de l'espace mémoire non-utilisé.
    analyzer_append(memory_analysis,arraylist_capacity(a)-arraylist_size(a));
  }

  // Affichage de quelques statistiques sur l'expérience.
  fprintf(stderr, "Total cost: %Lf\n", get_total_cost(time_analysis));
  fprintf(stderr, "Average cost: %Lf\n", get_average_cost(time_analysis));
  fprintf(stderr, "Variance: %Lf\n", get_variance(time_analysis));
  fprintf(stderr, "Standard deviation: %Lf\n", get_standard_deviation(time_analysis));

  // Sauvegarde les données de l'expérience.
  save_values(time_analysis, "../plots/dynamic_array_time_c.plot");
  save_values(copy_analysis, "../plots/dynamic_array_copy_c.plot");
  save_values(memory_analysis, "../plots/dynamic_array_memory_c.plot");

  // Nettoyage de la mémoire avant la sortie du programme
  arraylist_destroy(a);
  analyzer_destroy(time_analysis);
  analyzer_destroy(copy_analysis);
  analyzer_destroy(memory_analysis);
  return EXIT_SUCCESS;
}
