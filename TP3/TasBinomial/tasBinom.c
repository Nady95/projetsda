#include<stdio.h>
#include<malloc.h>
#include "tasBinom.h"

tasBinom* creer_tas_binomial(){
  tasBinom * tas;
  tas = NULL;
  return tas;
}

void lien_binomial(tasBinom * tas1, tasBinom * tas2){
  tas1->pere = tas2;
  tas1->frere = tas2->enfant;
  tas2->enfant = tas1;
  tas2->degres = tas2->degres + 1;
}

tasBinom* creer_noeud(int k){
  tasBinom* p;
  p = (tasBinom*) malloc(sizeof(struct noeud));
  p->cle = k;
  return p;
}

tasBinom * fusion_tas_binomiaux(tasBinom * tas1, tasBinom * tas2){
  tasBinom * t = creer_tas_binomial();
  tasBinom * y;
  tasBinom * z;
  tasBinom * a;
  tasBinom * b;
  y = tas1;
  z = tas2;
  if(y != NULL){
    if(z != NULL && y->degres <= z->degres)
      t = y;
    else if(z != NULL && y->degres > z->degres)
      t = z;
    else
      t = y;
  } else
    t = z;
  while(y != NULL && z != NULL){
    if(y->degres < z->degres){
      y = y->frere;
    } else if(y->degres == z->degres){
      a = y->frere;
      y->frere = z;
      y = a;
    } else {
      b = z->frere;
      z->frere = y;
      z = b;
    }
  }
  return t;
}

tasBinom * union_tas_binomiaux(tasBinom * tas1, tasBinom * tas2){
  tasBinom * t = creer_tas_binomial();
  tasBinom * avantX;
  tasBinom * apresX;
  tasBinom * x;
  t = fusion_tas_binomiaux(tas1, tas2);
  if(t == NULL)
    return t;
  avantX = NULL;
  x = t;
  apresX = x->frere;
  while (apresX != NULL){
    if((x->degres != apresX->degres) || ((apresX->frere != NULL) && (apresX->frere)->degres == x->degres)){
      avantX = x;
      x = apresX;
    }
    else{
      if(x->cle <= apresX->cle){
        x->frere = apresX->frere;
        lien_binomial(apresX, x);
      }
      else{
        if(avantX == NULL)
          t = apresX;
        else
          avantX->frere = apresX;
        lien_binomial(x, apresX);
        x = apresX;
      }
    }
    apresX = x->frere;
  }
  return t;
}

tasBinom * inserer_tas_binomial(tasBinom * T, tasBinom * x){
  tasBinom * D = creer_tas_binomial();
  x->pere = NULL;
  x->enfant = NULL;
  x->frere = NULL;
  x->degres = 0;
  D = x;
  T = union_tas_binomiaux(T, D);
  return T;
}
/*
tasBinom* extraire_min_binomial(tasBinom* tas1){
  int min;
  tasBinom* t = NULL;
  tasBinom* x = tas1;
  tasBinom* y;
  tasBinom* p;
  y = NULL;
  
  if(x == NULL)
    return x;
  
  min=x->cle;
  p = x;
  
  while(p->frere != NULL){
    if((p->frere)->cle < min){
      min = (p->frere)->cle;
      t = p;
      x = p->frere;
    }
    p = p->frere;
  }
  if(t == NULL && x->frere == NULL)
    tas1 = NULL;
  else if(t == NULL)
    tas1 = x->frere;
  else if(t->frere == NULL)
    t = NULL;
  else
    t->frere = x->frere;
  if(x->enfant != NULL){
    inverser_liste(x->enfant);
    (x->enfant)->frere = NULL;
  }
  return x;
}

void inverser_liste(tasBinom* y){
  if (y->frere != NULL) {
    inverser_liste(y->frere);
    (y->frere)->frere = y;
  } 
  else
    Hr = y;
}
*/