#ifndef __TASBINOM_H
#define __TASBINOM_H

#include <stddef.h>


typedef struct noeud {
    int cle;
    int degres;
    struct noeud* pere;
    struct noeud* enfant;
    struct noeud* frere;
} tasBinom;


tasBinom* creer_tas_binomial();
void lien_binomial(tasBinom*, tasBinom*);
tasBinom* creer_noeud(int);
tasBinom * fusion_tas_binomiaux(tasBinom*, tasBinom*);
tasBinom * union_tas_binomiaux(tasBinom*, tasBinom*);
tasBinom * inserer_tas_binomial(tasBinom*, tasBinom*);
//tasBinom* extraire_min_binomial(tasBinom*);
//void inverser_liste(tasBinom*);

#endif