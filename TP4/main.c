#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "bTree.c"
#include "avlTree.c"
#include "arraylist.h"
#include "analyzer.h"

int main(void){
  int i;
  // Analyse du temps pris par les opérations.
  analyzer_t * time_analysis = analyzer_create();
  // Analyse du nombre de copies faites par les opérations.
  analyzer_t * copy_analysis = analyzer_create();
  struct timespec before, after;
  clockid_t clk_id = CLOCK_REALTIME;
  int memory_allocation;
  
  bTree bt = btCreate();
  
  for(i = 0; i < 1000000 ; i++){
	clock_gettime(clk_id, &before);
    memory_allocation = btInsert(bt, i);
    clock_gettime(clk_id, &after);
	
	// Enregistrement du temps pris par l'opération
    analyzer_append(time_analysis, after.tv_nsec - before.tv_nsec);
    // Enregistrement du nombre de copies efféctuées par l'opération.
    analyzer_append(copy_analysis, (memory_allocation)? btGetsize(bt):1 );
  }
  
  // Affichage de quelques statistiques sur l'expérience.
  fprintf(stderr, "Total cost: %lf\n", get_total_cost(time_analysis));
  fprintf(stderr, "Average cost: %lf\n", get_average_cost(time_analysis));
  fprintf(stderr, "Variance: %lf\n", get_variance(time_analysis));
  fprintf(stderr, "Standard deviation: %lf\n", get_standard_deviation(time_analysis));

  // Sauvegarde les données de l'expérience.
  save_values(time_analysis, "../plots/bTree_insert_inc_time_c.plot");
  save_values(copy_analysis, "../plots/bTree_insert_inc_copy_c.plot");

  // Nettoyage de la mémoire avant la sortie du programme
  btDestroy(bt);
  analyzer_destroy(time_analysis);
  analyzer_destroy(copy_analysis);
  return EXIT_SUCCESS;
}
